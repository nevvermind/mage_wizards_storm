/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisManager;
import com.jadi.Wizard.ProductAttribute.Analysis.ShouldHaveNote;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Note;
import org.junit.Test;

import static org.junit.Assert.*;

public class AggregateTest
{
    @Test
    public void noteSuggestionIsTriggeredWhenAttributeHasNoNote() throws Exception
    {
        Attribute attr = new Attribute();
        attr.setCode("code");
        attr.addConfig(new Note().setValue("valid note"));

        AnalysisManager analysisManager = new AnalysisManager(attr);
        analysisManager.addAnalysis(new ShouldHaveNote());

        if (analysisManager.getAll().size() > 1) {
            fail("There should be no info about a valid note.");
        }
    }
}
