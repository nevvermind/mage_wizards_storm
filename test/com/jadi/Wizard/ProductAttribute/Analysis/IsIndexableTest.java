/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisResult;
import com.jadi.Wizard.ProductAttribute.Analysis.IsIndexable;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.BackendType;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.FrontendInput;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Filterable;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.FilterableInSearch;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.VisibleInAdvancedSearch;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

public class IsIndexableTest
{
    private IsIndexable isIndexableAnalysis;
    private Attribute attribute;

    @Before
    public void setUp() throws Exception
    {
        isIndexableAnalysis = new IsIndexable();
        attribute = new Attribute("foobar");
    }

    @Test
    public void testAttributeIsNotIndexableIfIsNotFilterable()
    {
        attribute.addConfig(new Filterable().setValue(Filterable.Type.NO));
        assertIndexable(false, "1");

        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.NO));
        assertIndexable(false, "2");

        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.NO));
        assertIndexable(false, "3");

        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.NONE));
        assertIndexable(false, "4");

        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.NONE));
        assertIndexable(false, "5");

        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        assertIndexable(false, "6");

        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_NO_RESULTS));
        assertIndexable(false, "7");

        attribute.addConfig(new Filterable().setValue(Filterable.Type.NONE));
        assertIndexable(false, "8");
    }

    @Test
    public void testAttributeIsIndexableOnCertainConfigurationSetup()
    {
        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.YES));
        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.YES));

        attribute.addConfig(new BackendType().setValue(BackendType.Type.INT));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.SELECT));
        assertIndexable(true, "1");

        attribute.addConfig(new BackendType().setValue(BackendType.Type.VARCHAR));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.MULTISELECT));
        assertIndexable(true, "2");

        attribute.addConfig(new BackendType().setValue(BackendType.Type.DECIMAL));
        assertIndexable(true, "3");

        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_NO_RESULTS));
        assertIndexable(true, "4");

        // not indexable...

        attribute.addConfig(new Filterable().setValue(Filterable.Type.NO));
        assertIndexable(false, "5");

        attribute.addConfig(new Filterable().setValue(Filterable.Type.NONE));
        assertIndexable(false, "6");

        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.TEXTAREA));
        assertIndexable(true, "7");

        // ----
        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.YES));
        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.YES));

        attribute.addConfig(new BackendType().setValue(BackendType.Type.INT));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.MULTISELECT));
        assertIndexable(false, "8");

        // ----
        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.YES));
        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.YES));

        attribute.addConfig(new BackendType().setValue(BackendType.Type.VARCHAR));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.SELECT));
        assertIndexable(false, "9");

        // ----
        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.YES));
        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.YES));

        attribute.addConfig(new BackendType().setValue(BackendType.Type.VARCHAR));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.SELECT));
        assertIndexable(false, "10");

        // ----
        attribute.addConfig(new Filterable().setValue(Filterable.Type.YES_RESULTS));
        attribute.addConfig(new FilterableInSearch().setValue(YesNoCombo.Type.YES));
        attribute.addConfig(new VisibleInAdvancedSearch().setValue(YesNoCombo.Type.YES));

        attribute.addConfig(new BackendType().setValue(BackendType.Type.INT));
        attribute.addConfig(new FrontendInput().setValue(FrontendInput.Type.MULTISELECT));
        assertIndexable(false, "11");
    }

    private void assertIndexable(boolean indexableFlag, String reason)
    {
        AnalysisResult result = isIndexableAnalysis.getResult(attribute);
        assertThat(result.getType(), is(AnalysisResult.Type.INFO));

        String bundleKey = "analysis.result.title." + (indexableFlag ? "is_indexable" : "is_not_indexable");

        if (reason == null) {
            assertThat(result.getTitle(), is(I18N.getString(bundleKey)));
        } else {
            assertThat(reason, result.getTitle(), is(I18N.getString(bundleKey)));
        }
    }
}
