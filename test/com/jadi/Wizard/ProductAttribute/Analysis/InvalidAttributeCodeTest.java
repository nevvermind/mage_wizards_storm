/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisManager;
import com.jadi.Wizard.ProductAttribute.Analysis.InvalidAttributeCode;
import com.jadi.Wizard.ProductAttribute.Attribute;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class InvalidAttributeCodeTest
{
    @Test
    public void passes_on_correct_attr_code()
    {
        AnalysisManager analysisManager;

        analysisManager = getAnalysisManagerWithAttributeCode("valid_code");
        assertOneOk(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("08_valid_code");
        assertOneOk(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("valid_code_08");
        assertOneOk(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("validCode");
        assertOneOk(analysisManager);
    }

    @Test
    public void fails_on_underscores_at_beginning()
    {
        AnalysisManager analysisManager = getAnalysisManagerWithAttributeCode("_invalid_code");

        assertOneError(analysisManager);
        assertThat(
            analysisManager.getErrors().get(0).getDescription(),
            is(I18N.getString("analysis.invalid_attr_code.desc.invalid_attr_char_start"))
        );

        analysisManager = getAnalysisManagerWithAttributeCode("_");
        assertOneError(analysisManager);
    }

    @Test
    public void fails_on_trailing_underscores()
    {
        AnalysisManager analysisManager = getAnalysisManagerWithAttributeCode("invalid_code_");

        assertOneError(analysisManager);
        assertThat(
            analysisManager.getErrors().get(0).getDescription(),
            is(I18N.getString("analysis.invalid_attr_code.desc.invalid_attr_char_end"))
        );
    }

    @Test
    public void fails_on_only_whitespace()
    {
        AnalysisManager analysisManager = getAnalysisManagerWithAttributeCode("   code with space  ");
        assertOneError(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("     ");
        assertOneError(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("");
        assertOneError(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("\r\n\r\t");
        assertOneError(analysisManager);
    }

    @Test
    public void fails_on_invalid_char()
    {
        AnalysisManager analysisManager = getAnalysisManagerWithAttributeCode("invalid_(attr)");
        assertOneError(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("invalid_(attr");
        assertOneError(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("(invalid_(attr");
        assertOneError(analysisManager);

        analysisManager = getAnalysisManagerWithAttributeCode("invalid_attr)");
        assertOneError(analysisManager);
    }

    private AnalysisManager getAnalysisManagerWithAttributeCode(String code)
    {
        Attribute attr = new Attribute(code);
        AnalysisManager analysisManager = new AnalysisManager(attr);
        analysisManager.addAnalysis(new InvalidAttributeCode());

        return analysisManager;
    }

    private void assertOneOk(AnalysisManager analysisManager)
    {
        assertThat(analysisManager.getAll().size(), is(1));
        assertThat(analysisManager.getOks().size(), is(1));
        assertThat(analysisManager.getErrors().size(), is(0));
    }

    private void assertOneError(AnalysisManager analysisManager)
    {
        assertThat(analysisManager.getAll().size(), is(1));
        assertThat(analysisManager.getOks().size(), is(0));
        assertThat(analysisManager.getErrors().size(), is(1));
    }
}
