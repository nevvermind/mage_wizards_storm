/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.*;
import com.jadi.Wizard.ProductAttribute.PhpArrayRenderer;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class PhpArrayRendererTest
{
    @Test
    public void testRenderedPhpArray() throws Exception
    {
        Attribute attr = new Attribute();
        attr.setCode("code");
        attr.addConfig(new Label().setValue("labe'l2"));
        attr.addConfig(new Global().setValue(Global.Scope.GLOBAL));
        attr.addConfig(new BackendModel().setValue(BackendModel.Type.DATETIME));
        attr.addConfig(new IsRequired().setValue(YesNoCombo.Type.NO));
        attr.addConfig(new SourceModel().setValue(SourceModel.Type.NONE));
        attr.addConfig(new Options().setValue("option1\nopti'on2"));

        String actualPhpCode = "/* @var $installer Mage_Catalog_Model_Resource_Setup */\n" +
                "// $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');\n" +
                "// $installer->startSetup();\n" +
                "\n" +
                "$installer->addAttribute(\n" +
                "    Mage_Catalog_Model_Product::ENTITY,\n" +
                "    'code',\n" +
                "    array(\n" +
                "        'label' => 'labe\\'l2',\n" +
                "        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,\n" +
                "        'backend' => 'eav/entity_attribute_backend_datetime',\n" +
                "        'required' => false,\n" +
                "        'option' => array(\n" +
                "            'values' => array(\n" +
                "                'option1',\n" +
                "                'opti\\'on2',\n" +
                "            ),\n" +
                "        ),\n" +
                "    )\n" +
                ");\n" +
                "\n" +
                "// $installer->endSetup();\n";

        assertThat(new PhpArrayRenderer().render(attr), is(actualPhpCode));
    }
}
