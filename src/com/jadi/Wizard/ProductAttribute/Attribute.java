/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute;

import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Options;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.NullAttributeConfig;

import java.util.LinkedHashMap;
import java.util.Map;

public class Attribute
{
    private String code;

    private Map<String, AttributeConfig> attributeConfigs = new LinkedHashMap<>();

    public Attribute()
    {}

    public Attribute(String code)
    {
        this.code = code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    public void addConfig(AttributeConfig attributeConfig)
    {
        attributeConfig.setAttribute(this);
        attributeConfigs.put(attributeConfig.getCode(), attributeConfig);
    }

    private Map<String, AttributeConfig> getAttributeConfigs()
    {
        return attributeConfigs;
    }

    public AttributeConfig getConfig(String attrConfigCode)
    {
        AttributeConfig attrConfig = getAttributeConfigs().get(attrConfigCode);

        if (attrConfig == null) {
            return new NullAttributeConfig();
        }

        return attrConfig;
    }

    public void removeConfig(AttributeConfig attrConfig)
    {
        attributeConfigs.remove(attrConfig.getCode());
    }

    public Map<String, AttributeConfig> getAllConfigs()
    {
        return getAttributeConfigs();
    }

    public boolean hasOptions()
    {
        AttributeConfig options = getConfig(Options.CODE);

        return !(options instanceof NullAttributeConfig) &&
               !((Options) options).getOptions().isEmpty();
    }
}
