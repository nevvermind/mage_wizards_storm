/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisResult;
import com.jadi.Wizard.ProductAttribute.Analysis.InvalidAttributeCode;
import com.jadi.Wizard.ProductAttribute.Analysis.NoLabel;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Options;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.NullAttributeConfig;
import org.apache.commons.lang.StringUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

public class PhpArrayRenderer implements AttributeRenderer
{
    private static final String TPL =
"/* @var $installer Mage_Catalog_Model_Resource_Setup */\n" +
"// $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');\n" +
"// $installer->startSetup();\n\n" +
"$installer->addAttribute(\n" +
"    Mage_Catalog_Model_Product::ENTITY,\n" +
"    %s,\n" +
"    array(\n" +
"%s" +
"    )\n" +
");\n\n" +
"// $installer->endSetup();\n"
;
    public String render(Attribute attribute)
    {
        if (new InvalidAttributeCode().getResult(attribute).getType() != AnalysisResult.Type.OK) {
            return I18N.getString("render.err.invalid_attr_code");
        }

        if (new NoLabel().getResult(attribute).getType() != AnalysisResult.Type.OK) {
            return I18N.getString("render.err.no_attr_label");
        }

        String attrCode = attribute.getCode().trim();
        StringBuilder attrStringBuilder = new StringBuilder();

        Iterator<Map.Entry<String, AttributeConfig>> it =
                attribute.getAllConfigs().entrySet().iterator();

        while (it.hasNext()) {
            AttributeConfig attrConfig = it.next().getValue();

            /**
             * TODO - instead of this, maybe we can use a rendering policy
             * AttributeConfig.setRenderPolicy(RenderPolicy)
             *
             * "canRender" should be in that policy.
             * So should "toPhpValue" and "toPhpString"
             */
            if (!attrConfig.canRender() || attrConfig instanceof Options) { // render Options last
                continue;
            }

            attrStringBuilder.append(getAttrConfigAsPhpArrayElement(attrConfig, 8))
                             .append("\n");
        }

        if (!(attribute.getConfig(Options.CODE) instanceof NullAttributeConfig)) {
            Options optionConfig = (Options) attribute.getConfig(Options.CODE);
            List<String> options = optionConfig.getOptions();
            if (!options.isEmpty()) {
                attrStringBuilder.append(renderOptions(optionConfig))
                                 .append("\n");
            }
        }

        if (!PhpArrayRenderer.isPhpConstant(attrCode)) {
            attrCode = "'" + PhpArrayRenderer.escapeSingleQuotes(attrCode) + "'";
        }
        return String.format(TPL, attrCode, attrStringBuilder.toString());
    }

    public static boolean isPhpConstant(String value)
    {
        return value.matches("\\w+::\\w+");
    }

    public static String escapeSingleQuotes(String value)
    {
        return value.replaceAll("'", Matcher.quoteReplacement("\\") + "'");
    }

    private String getAttrConfigAsPhpArrayElement(AttributeConfig attrConfig, int spaceIndent)
    {
        return asPhpArrayElement("'" + attrConfig.getCode() + "'", attrConfig.toPhpValue(), spaceIndent);
    }

    private String asPhpArrayElement(Object key, Object value, int spaceIndent)
    {
        return StringUtils.repeat(" ", spaceIndent) +
               String.format("%s => %s,", key.toString(), value.toString());
    }

    private String renderOptions(Options options)
    {
        final String indent16 = getIndent(16);
        final String indent12 = getIndent(12);
        final String indent8 = getIndent(8);

        StringBuilder phpString = new StringBuilder("array(\n" + indent12 + "'values' => array(\n");
        for (String value : options.getOptions()) {
            String arrayElem = indent16 + "'" + escapeSingleQuotes(value) + "',\n";
            phpString.append(arrayElem);
        }

        phpString.append(indent12 + "),\n" + indent8 + "),");

        return indent8 + "'" + Options.CODE + "' => " + phpString.toString();
    }

    private String getIndent(int indent)
    {
        return StringUtils.repeat(" ", indent);
    }
}
