/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Wizard.ProductAttribute.Attribute;

import java.util.*;

public class AnalysisManager
{
    private final Attribute attribute;
    private List<IAnalysis> analysisList = new ArrayList<>();

    private final Map<AnalysisResult.Type, List<AnalysisResult>> resultsByType = new LinkedHashMap<>();
    private boolean alreadyExecuted = false;

    public AnalysisManager(Attribute attribute)
    {
        this.attribute = attribute;
        initResultsList();
    }

    private void initResultsList()
    {
        resultsByType.put(AnalysisResult.Type.OK, new ArrayList<>());
        resultsByType.put(AnalysisResult.Type.INFO, new ArrayList<>());
        resultsByType.put(AnalysisResult.Type.WARNING, new ArrayList<>());
        resultsByType.put(AnalysisResult.Type.ERROR, new ArrayList<>());
    }

    public static AnalysisManager withAllAnalyses(Attribute attribute)
    {
        AnalysisManager analysisManager = new AnalysisManager(attribute);

        analysisManager.addAnalysis(new InvalidAttributeCode());
        analysisManager.addAnalysis(new NoLabel());
        analysisManager.addAnalysis(new ShouldHaveNote());
        analysisManager.addAnalysis(new IsIndexable());
        analysisManager.addAnalysis(new IsConfigurable());
        analysisManager.addAnalysis(new NoAttributeSetInsertion());
        analysisManager.addAnalysis(new SelectOrMultiselectOptionsBackend());
        analysisManager.addAnalysis(new SourceOrOptions());

        return analysisManager;
    }

    public void addAnalysis(IAnalysis analysis)
    {
        analysisList.add(analysis);
    }

    private void execute(Attribute attribute)
    {
        if (alreadyExecuted) {
            return;
        }

        for (IAnalysis analysis : analysisList) {

            AnalysisResult result = analysis.getResult(attribute);

            if (resultsByType.containsKey(result.getType())) {
                resultsByType.get(result.getType()).add(result);
            }
        }

        alreadyExecuted = true;
    }

    public List<AnalysisResult> getErrors()
    {
        execute(attribute);
        return resultsByType.get(AnalysisResult.Type.ERROR);
    }

    public List<AnalysisResult> getWarnings()
    {
        execute(attribute);
        return resultsByType.get(AnalysisResult.Type.WARNING);
    }

    public List<AnalysisResult> getInfos()
    {
        execute(attribute);
        return resultsByType.get(AnalysisResult.Type.INFO);
    }

    public List<AnalysisResult> getOks()
    {
        execute(attribute);
        return resultsByType.get(AnalysisResult.Type.OK);
    }

    public List<AnalysisResult> getAll()
    {
        execute(attribute);

        ArrayList<AnalysisResult> all = new ArrayList<>();

        // sorted by urgency
        all.addAll(resultsByType.get(AnalysisResult.Type.ERROR));
        all.addAll(resultsByType.get(AnalysisResult.Type.WARNING));
        all.addAll(resultsByType.get(AnalysisResult.Type.INFO));
        all.addAll(resultsByType.get(AnalysisResult.Type.OK));

        return all;
    }

    public void reset()
    {
        alreadyExecuted = false;
        resultsByType.clear();
        initResultsList();
    }
}
