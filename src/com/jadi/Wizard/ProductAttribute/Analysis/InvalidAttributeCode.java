/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Attribute;
import org.apache.commons.lang.StringUtils;

public class InvalidAttributeCode implements IAnalysis
{
    @Override
    public AnalysisResult getResult(Attribute attribute)
    {
        if (StringUtils.isBlank(attribute.getCode())) {
            return error(I18N.getString("analysis.invalid_attr_code.desc.no_attr_code"));
        }

        if (!attribute.getCode().equals(StringUtils.deleteWhitespace(attribute.getCode()))) {
            return error(I18N.getString("analysis.invalid_attr_code.desc.attr_with_whitespace"));
        }

        if (attribute.getCode().matches(".*[^A-Za-z0-9_].*")) {
            return error(I18N.getString("analysis.invalid_attr_code.desc.invalid_attr_char"));
        }

        if (attribute.getCode().matches("^[^A-Za-z0-9].*")) {
            return error(I18N.getString("analysis.invalid_attr_code.desc.invalid_attr_char_start"));
        }

        if (attribute.getCode().matches(".*[^A-Za-z0-9]$")) {
            return error(I18N.getString("analysis.invalid_attr_code.desc.invalid_attr_char_end"));
        }

        return AnalysisResult.ok(I18N.getString("analysis.invalid_attr_code.title.ok"));
    }

    private AnalysisResult error(String description)
    {
        return new AnalysisResult(
            AnalysisResult.Type.ERROR,
            I18N.getString("analysis.invalid_attr_code.title.invalid"),
            description
        );
    }
}
