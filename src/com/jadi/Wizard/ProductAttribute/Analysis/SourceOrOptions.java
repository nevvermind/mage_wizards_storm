/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.SourceModel;

public class SourceOrOptions implements IAnalysis
{
    @Override
    public AnalysisResult getResult(Attribute attribute)
    {
        AttributeConfig sourceModel = attribute.getConfig(SourceModel.CODE);

        Object sourceModelVal = sourceModel.getValue();
        if (attribute.hasOptions() &&
            !(sourceModelVal.equals(SourceModel.Type.NONE) || sourceModelVal.equals(SourceModel.Type.EAV_TABLE))
        ) {
            return notOk();
        }

        return AnalysisResult.ok("Either source model or options");
    }

    private AnalysisResult notOk()
    {
        return new AnalysisResult(
            AnalysisResult.Type.WARNING,
            "Both source model and options have been set",
            "The source model will override the options set for the attribute. " +
            "Consider choosing one or the other, or make sure the source model extends the " +
            "Mage_Eav_Model_Entity_Attribute_Source_Table class."
        );
    }
}
