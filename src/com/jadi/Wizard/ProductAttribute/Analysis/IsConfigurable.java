/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.FrontendInput;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Global;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.IsUserDefined;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.SourceModel;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Configurable;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Visible;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;

public class IsConfigurable implements IAnalysis
{
    @Override
    public AnalysisResult getResult(Attribute attribute)
    {
        AttributeConfig scope = attribute.getConfig(Global.CODE);
        AttributeConfig visible = attribute.getConfig(Visible.CODE);
        AttributeConfig configurable = attribute.getConfig(Configurable.CODE);
        AttributeConfig userDefined = attribute.getConfig(IsUserDefined.CODE);

        if (scope.getValue() == Global.Scope.GLOBAL &&
            visible.getValue() == YesNoCombo.Type.YES &&
            configurable.getValue() == YesNoCombo.Type.YES &&
            userDefined.getValue() == YesNoCombo.Type.YES &&
            usesSource(attribute)
        ) {
            return AnalysisResult.ok("Attribute is Configurable-ready");
        }

        return new AnalysisResult(AnalysisResult.Type.INFO, "Attribute is not Configurable-ready");
    }

    private boolean usesSource(Attribute attribute)
    {
        AttributeConfig frontendInput = attribute.getConfig(FrontendInput.CODE);
        AttributeConfig sourceModel = attribute.getConfig(SourceModel.CODE);

        return frontendInput.getValue() == FrontendInput.Type.SELECT ||
               frontendInput.getValue() == FrontendInput.Type.MULTISELECT ||
               sourceModel.getValue() != SourceModel.Type.NONE;
    }
}
