/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.BackendType;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.FrontendInput;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Filterable;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.FilterableInSearch;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.VisibleInAdvancedSearch;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;

/**
 * Whether the attribute is indexable by the Product Attribute indexer.
 *
 * @link https://github.com/varinen/solvingmagento_1.7.0/blob/master/app/code/core/Mage/Catalog/Model/Resource/Eav/Attribute.php#L349
 */
public class IsIndexable implements IAnalysis
{
    @Override
    public AnalysisResult getResult(Attribute attribute)
    {
        AttributeConfig filterableInSearch = attribute.getConfig(FilterableInSearch.CODE);
        AttributeConfig visibleInAdvSearch = attribute.getConfig(VisibleInAdvancedSearch.CODE);
        AttributeConfig filterable = attribute.getConfig(Filterable.CODE);

        if (filterable.getValue() == Filterable.Type.NO ||
            filterable.getValue() == Filterable.Type.NONE ||
            visibleInAdvSearch.getValue() != YesNoCombo.Type.YES ||
            filterableInSearch.getValue() != YesNoCombo.Type.YES
        ) {
            return getNotIndexableResult();
        }

        AttributeConfig backendType = attribute.getConfig(BackendType.CODE);
        AttributeConfig frontendInput = attribute.getConfig(FrontendInput.CODE);

        // int + select
        if (backendType.getValue().equals(BackendType.Type.INT) &&
            frontendInput.getValue().equals(FrontendInput.Type.SELECT)
        ) {
            return getIndexableResult();
        }

        // varchar + multiselect
        if (backendType.getValue().equals(BackendType.Type.VARCHAR) &&
            frontendInput.getValue().equals(FrontendInput.Type.MULTISELECT)
        ) {
            return getIndexableResult();
        }

        // decimal
        if (backendType.getValue().equals(BackendType.Type.DECIMAL)
        ) {
            return getIndexableResult();
        }

        return getNotIndexableResult();
    }

    private AnalysisResult getNotIndexableResult()
    {
        return new AnalysisResult(
            AnalysisResult.Type.INFO,
            I18N.getString("analysis.result.title.is_not_indexable"),
            ""
        );
    }

    private AnalysisResult getIndexableResult()
    {
        return new AnalysisResult(
            AnalysisResult.Type.INFO,
            I18N.getString("analysis.result.title.is_indexable"),
            ""
        );
    }
}
