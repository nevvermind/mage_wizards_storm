/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.BackendType;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.FrontendInput;

/**
 * If the user adds options and:
 * a) selects "select" FE type, backend type SHOULD be "int"
 * b) multiselects "multiselect" FE type, backend type SHOULD be "varchar"
 */
public class SelectOrMultiselectOptionsBackend implements IAnalysis
{
    @Override
    public AnalysisResult getResult(Attribute attribute)
    {
        AttributeConfig frontendInput = attribute.getConfig(FrontendInput.CODE);
        AttributeConfig backendType= attribute.getConfig(BackendType.CODE);

        if (!attribute.hasOptions()) {
            return ok();
        }

        if (frontendInput.getValue().equals(FrontendInput.Type.SELECT) &&
            !backendType.getValue().equals(BackendType.Type.INT)
        ) {
            return notOkSelect();
        }

        if (frontendInput.getValue().equals(FrontendInput.Type.MULTISELECT) &&
            !backendType.getValue().equals(BackendType.Type.VARCHAR)
        ) {
            return notOkMultiselect();
        }

        return ok();
    }

    private AnalysisResult ok()
    {
        return AnalysisResult.ok("Options and select/multiselect backend");
    }

    private AnalysisResult notOkSelect()
    {
        return new AnalysisResult(
            AnalysisResult.Type.WARNING,
            "Sub-optimal Backend type",
            "It's better to have an \"int\" backend type whenever using " +
            "options and a \"select\" frontend type "
        );
    }

    private AnalysisResult notOkMultiselect()
    {
        return new AnalysisResult(
            AnalysisResult.Type.WARNING,
            "Sub-optimal Backend type",
            "It's better to have a \"varchar\" backend type whenever using " +
            "options and a \"multiselect\" frontend type "
        );
    }
}
