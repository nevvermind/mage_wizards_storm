/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Analysis;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Group;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.IsUserDefined;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;
import org.apache.commons.lang.StringUtils;

/**
 * As seen in <code>\Mage_Eav_Model_Entity_Setup::addAttribute()</code>:
 * <code>
 *     if (!empty($attr['group']) || empty($attr['user_defined'])) { ... add to attribute sets }
 * </code>
 *
 * @link http://www.webguys.de/magento/eav-attribute-setup/
 */
public class NoAttributeSetInsertion implements IAnalysis
{
    @Override
    public AnalysisResult getResult(Attribute attribute)
    {
        AttributeConfig userDefined = attribute.getConfig(IsUserDefined.CODE);
        AttributeConfig group = attribute.getConfig(Group.CODE);

        boolean hasGroup = !StringUtils.isEmpty(group.getValue().toString());

        if (hasGroup || userDefined.getValue() != YesNoCombo.Type.YES) {
            String description = I18N.getString("analysis.no_attr_set.desc.user_defined");
            if (hasGroup) {
                description = I18N.getString("analysis.no_attr_set.desc.with_group");
            }
            return new AnalysisResult(
                AnalysisResult.Type.OK,
                I18N.getString("analysis.no_attr_set.title.ok"),
                description
            );
        }

        return new AnalysisResult(
            AnalysisResult.Type.WARNING,
            I18N.getString("analysis.no_attr_set.title.not_ok"),
            I18N.getString("analysis.no_attr_set.desc.not_ok")
        );
    }
}
