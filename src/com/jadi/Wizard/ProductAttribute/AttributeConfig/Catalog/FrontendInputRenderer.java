/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

public class FrontendInputRenderer
    extends AttributeConfig<FrontendInputRenderer.Type>
    implements IPredefinedValues<FrontendInputRenderer.Type>,
               IUiRenderable<FrontendInputRenderer.ComboBox>
{
    public static final String COL  = "frontend_input_renderer";
    public static final String CODE = "input_renderer";

    public FrontendInputRenderer(Type value)
    {
        setValue(value);
    }

    public FrontendInputRenderer()
    {
    }

    public enum Type
    {
        NONE(AttributeConfig.OMIT_LABEL),
        BUTTON("button"),
        CHECKBOX("checkbox"),
        CHECKBOXES("checkboxes"),
        COLLECTION("collection"),
        COLUMN("column"),
        DATE("date"),
        EDITOR("editor"),
        FIELDSET("fieldset"),
        FILE("file"),
        GALLERY("gallery"),
        HIDDEN("hidden"),
        IMAGE("image"),
        IMAGE_FILE("imagefile"),
        LABEL("label"),
        LINK("link"),
        MULTILINE("multiline"),
        MULTISELECT("multiselect"),
        NOTE("note"),
        OBSCURE("obscure"),
        PASSWORD("password"),
        RADIO("radio"),
        RADIOS("radios"),
        RESET("reset"),
        SELECT("select"),
        SUBMIT("submit"),
        TEXT("text"),
        TEXTAREA("textarea"),
        TIME("time");

        private String value;

        Type(String configValue)
        {
            this.value = configValue;
        }

        @Override
        public String toString()
        {
            return value;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.frontend_input_renderer");
    }

    @Override
    public Type getDefault()
    {
        return Type.NONE;
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public Type[] getPredefinedValues()
    {
        return new Type[] {
            Type.NONE,
            Type.BUTTON,
            Type.CHECKBOX,
            Type.CHECKBOXES,
            Type.COLLECTION,
            Type.COLUMN,
            Type.DATE,
            Type.EDITOR,
            Type.FIELDSET,
            Type.FILE,
            Type.GALLERY,
            Type.HIDDEN,
            Type.IMAGE,
            Type.IMAGE_FILE,
            Type.LABEL,
            Type.LINK,
            Type.MULTILINE,
            Type.MULTISELECT,
            Type.NOTE,
            Type.OBSCURE,
            Type.PASSWORD,
            Type.RADIO,
            Type.RADIOS,
            Type.RESET,
            Type.SELECT,
            Type.SUBMIT,
            Type.TEXT,
            Type.TEXTAREA,
            Type.TIME
        };
    }

    @Override
    public String toPhpValue()
    {
        return toPhpString();
    }

    @Override
    public ComboBox getUiComponent()
    {
        return new ComboBox(this);
    }

    public class ComboBox extends JComboBox<FrontendInputRenderer.Type>
    {
        private FrontendInputRenderer attrConfig;

        ComboBox(FrontendInputRenderer attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(Type.NONE);
        }

        public FrontendInputRenderer getAttrConfig()
        {
            return attrConfig;
        }
    }
}
