/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

/**
 * See the <code>\Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_*</code> constants.
 * For some reason, the constants were created in Mage_Catalog.
 */
public class Filterable
    extends AttributeConfig<Filterable.Type>
    implements IPredefinedValues<Filterable.Type>,
    IUiRenderable<Filterable.ComboBox>
{
    public static final String COL  = "is_filterable";
    public static final String CODE = "filterable";

    public Filterable(Type value)
    {
        setValue(value);
    }

    public Filterable()
    {
    }

    public enum Type
    {
        NONE("", AttributeConfig.OMIT_LABEL),
        NO("0", "No"),
        YES_RESULTS("1", "Yes (with results)"),
        YES_NO_RESULTS("2", "Yes (no results)");

        private String numericVal;
        private String friendlyName;

        Type(String numericVal, String friendlyName)
        {
            this.numericVal = numericVal;
            this.friendlyName = friendlyName;
        }

        @Override
        public String toString()
        {
            return toFriendlyName();
        }

        public String toNumericValue()
        {
            return numericVal;
        }

        public String toFriendlyName()
        {
            return friendlyName;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.filterable");
    }

    @Override
    public Type getDefault()
    {
        return Type.NONE;
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public Type[] getPredefinedValues()
    {
        return new Type[] {
            Type.NONE,
            Type.NO,
            Type.YES_RESULTS,
            Type.YES_NO_RESULTS
        };
    }

    @Override
    public String toPhpValue()
    {
        return getValue().toNumericValue();
    }

    @Override
    public ComboBox getUiComponent()
    {
        return new ComboBox(this);
    }

    public class ComboBox extends JComboBox<Type>
    {
        private Filterable attrConfig;

        ComboBox(Filterable attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(attrConfig.getDefault());
        }

        public Filterable getAttrConfig()
        {
            return attrConfig;
        }
    }
}
