/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;

public class Searchable
    extends AttributeConfig<YesNoCombo.Type>
    implements IUiRenderable<YesNoCombo>
{
    public static final String COL  = "is_searchable";
    public static final String CODE = "searchable";

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.searchable");
    }

    @Override
    public YesNoCombo.Type getDefault()
    {
        return YesNoCombo.Type.NONE;
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public YesNoCombo getUiComponent()
    {
        return new YesNoCombo(this);
    }
}
