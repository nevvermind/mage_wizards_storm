/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.PhpArrayRenderer;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Every attribute config has a internal name which maps to an actual table column.
 *
 * For example, this is taken from <code>\Mage_Eav_Model_Entity_Setup::_prepareValues()</code>:
 * <code>
 * $data = array(
 *     'backend_model'   => $this->_getValue($attr, 'backend'),
 *     'backend_type'    => $this->_getValue($attr, 'type', 'varchar'),
 *     'backend_table'   => $this->_getValue($attr, 'table'),
 *     'frontend_model'  => $this->_getValue($attr, 'frontend'),
 *     'frontend_input'  => $this->_getValue($attr, 'input', 'text'),
 *     'frontend_label'  => $this->_getValue($attr, 'label'),
 *     'frontend_class'  => $this->_getValue($attr, 'frontend_class'),
 *     'source_model'    => $this->_getValue($attr, 'source'),
 *     'is_required'     => $this->_getValue($attr, 'required', 1),
 *     'is_user_defined' => $this->_getValue($attr, 'user_defined', 0),
 *     'default_value'   => $this->_getValue($attr, 'default'),
 *     'is_unique'       => $this->_getValue($attr, 'unique', 0),
 *     'note'            => $this->_getValue($attr, 'note'),
 *     'is_global'       => $this->_getValue($attr, 'global', 1),
 * );
 * </code>
 *
 * The attribute config code is "backend", and the table column name is "backend_model".
 *
 * Magento relies on a base table - "eav_attribute" - and, for the rest of the specific attribute configs,
 * like "product" or "customer", specific tables are used, like "catalog_eav_attribute" or "customer_eav_attribute".
 */
abstract public class AttributeConfig<T> implements IPhpValue
{
    public static final String OMIT_LABEL = "<OMIT>";

    private T value;
    private Attribute attribute;

    abstract public String getColumnName();

    abstract public String getCode();

    abstract public String getLabelText();

    abstract public T getDefault();

    abstract public boolean isValid();

    public void setAttribute(@NotNull Attribute attribute)
    {
        this.attribute = attribute;
    }

    public final Attribute getAttribute()
    {
        return attribute;
    }

    public final T getValue()
    {
        return value;
    }

    public final AttributeConfig<T> setValue(T value)
    {
        this.value = value;
        return this;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) {
            return false;
        }

        if (obj instanceof AttributeConfig) {
            AttributeConfig config = (AttributeConfig) obj;
            return config.getColumnName().equals(this.getColumnName()) &&
                   config.getCode().equals(this.getCode()) &&
                   config.getValue().equals(this.getValue());
        }

        return false;
    }

    @Override
    public String toPhpValue()
    {
        if (!canRender()) {
            return "";
        }

        if (value instanceof String) {
            return toPhpString();
        } else if (value instanceof YesNoCombo.Type) {
            return getValue() == YesNoCombo.Type.YES ? "true" : "false";
        }
        return getValue().toString();
    }

    public String toPhpString()
    {
        if (!canRender()) {
            return "";
        }
        return "'" + PhpArrayRenderer.escapeSingleQuotes(getValue().toString()) + "'";
    }

    public boolean canRender()
    {
        boolean isEmpty = getValue().toString().equals(AttributeConfig.OMIT_LABEL) ||
                          StringUtils.isEmpty(getValue().toString());

        return !isEmpty;
    }
}
