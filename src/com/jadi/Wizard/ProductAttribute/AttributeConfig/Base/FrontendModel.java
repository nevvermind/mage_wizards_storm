/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

/**
 http://www.solvingmagento.com/magento-eav-system/#frontend_model

 EAV Attribute Property frontend_model

 Another important property of the EAV attribute metadata is frontend_model. This property connects attributes to classes, which define how the attribute is rendered in HTML. Don’t let the word frontend confuse you – it is not about how the attribute is presented to customers in the shop’s “front end”, but about the rendering of the attribute input fields in the administration “back end”. Thus, while the backend_model classes’ work is saving and retrieving the attribute data, frontend_model s are responsible for the data presentation to the user.

 Class Mage_Eav_Model_Entity_Attribute_Frontend_Abstract provides a common functionality for attribute frontend models. Some of its methods deserve more attention:

 getClass() – generates a CSS class for the attribute’s input HTML element. If an attribute is required, its input field receives a CSS class required. Other CSS classes can be taken from the attribute’s frontend_class property (stored in the eav_attribute table). Some EAV entities in Magento extend their attribute metadata with a validate_rules property. This property is used to produce additional CSS classes that can be picked up by a JavaScript validator (validate-email, validate-url etc.). Validate rules are stored as serialized associative arrays:

 array(
 'max_text_length'   => 255,
 'min_text_length'   => 1
 )

 getInputRendererClass() – the catalog entities (catalog_category and catalog_product) have an additional attribute property called frontend_input_renderer. It stores configuration references to Magento block classes that extend the HTML output of the attribute’s form input elements. One such example is Mage_Adminhtml_Block_Catalog_Category_Helper_Sortby_Available (look into its function getElementHtml())
 getSelectOptions() – returns you an associated array containing attribute option values and their labels. Very useful when you need to generate a drop down or a multiselect field. Naturally will work only with select or multiselect attributes.

 There are 10 classes extending the abstract frontend model. I do not count the frontend model classes in the Mage_Sales module, because I have never encountered them in action. I presume, they are left for backward compatibility, since in the current Magento version (1.7.0) the sales entities use flat tables to store their attribute data. The following diagram show the inheritance relationships of the Magento’s frontend models.

 EAV attribute frontend models

 The default frontend model is represented by the class Mage_Eav_Model_Entity_Attribute_Frontend_Default. This class doesn’t contain any overridden functions of properties, but simply references to the implementation in the abstract Mage_Eav_Model_Entity_Attribute_Frontend_Abstract class. Every attribute gets the default frontend model automatically, unless it has an explicit setting in the frontend_model column of the table eav_attribute. In a clean installation of Magento Community 1.7.0 only four attributes have custom defined frontend models:

 customer_entity->dob: Mage_Eav_Model_Entity_Attribute_Frontend_Datetime
 catalog_product_entity->image, catalog_product_entity->small_image, and catalog_product_entity->thumbnail: Mage_Catalog_Model_Product_Attribute_Frontend_Image

 */
// TODO - research this config more in depth; are there any values predefined?
public class FrontendModel extends AttributeConfig<String> implements IUiRenderable<JTextField>
{
    public static final String COL  = "frontend_model";
    public static final String CODE = "frontend";

    public FrontendModel(String value)
    {
        setValue(value);
    }

    public FrontendModel()
    {
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.frontend_model");
    }

    @Override
    public String getDefault()
    {
        return "";
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public JTextField getUiComponent()
    {
        return new JTextField();
    }

    @Override
    public boolean canRender()
    {
        return true;
    }
}
