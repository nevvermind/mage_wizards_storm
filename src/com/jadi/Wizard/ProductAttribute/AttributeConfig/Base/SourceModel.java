/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

public class SourceModel
    extends AttributeConfig<SourceModel.Type>
    implements IPredefinedValues<SourceModel.Type>,
               IUiRenderable<SourceModel.SourceModelCombo>
{
    public static final String COL  = "source_model";
    public static final String CODE = "source";

    public SourceModel(Type value)
    {
        setValue(value);
    }

    public SourceModel()
    {
    }

    public enum Type
    {
        NONE(AttributeConfig.OMIT_LABEL),
        EAV_BOOLEAN("eav/entity_attribute_source_boolean"),
        EAV_CONFIG("eav/entity_attribute_source_config"),
        EAV_STORE("eav/entity_attribute_source_store"),
        EAV_TABLE("eav/entity_attribute_source_table"),
        CUSTOMER_COUNTRY("customer/address_attribute_source_country"),
        CUSTOMER_REGION("customer/address_attribute_source_region"),
        CUSTOMER_WEBSITE("customer/customer_attribute_source_website");

        private String value;

        Type(String configValue)
        {
            this.value = configValue;
        }

        @Override
        public String toString()
        {
            return value;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.source_model");
    }

    @Override
    public Type getDefault()
    {
        return Type.NONE;
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public Type[] getPredefinedValues()
    {
        return new Type[] {
            Type.NONE,
            Type.EAV_BOOLEAN,
            Type.EAV_CONFIG,
            Type.EAV_STORE,
            Type.EAV_TABLE,
            Type.CUSTOMER_COUNTRY,
            Type.CUSTOMER_REGION,
            Type.CUSTOMER_WEBSITE,
        };
    }

    @Override
    public String toPhpValue()
    {
        return toPhpString();
    }

    @Override
    public SourceModelCombo getUiComponent()
    {
        return new SourceModelCombo(this);
    }

    public class SourceModelCombo extends JComboBox<SourceModel.Type>
    {
        private SourceModel attrConfig;

        SourceModelCombo(SourceModel attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(attrConfig.getDefault());
        }

        public SourceModel getAttrConfig()
        {
            return attrConfig;
        }
    }
}
