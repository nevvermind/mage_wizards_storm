/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

public class FrontendClass
    extends AttributeConfig<FrontendClass.Type>
    implements IPredefinedValues<FrontendClass.Type>,
               IUiRenderable<FrontendClass.FrontendClassCombo>
{
    // they're the same
    public static final String COL  = "frontend_class";
    public static final String CODE = "frontend_class";

    public FrontendClass(Type value)
    {
        setValue(value);
    }

    public FrontendClass()
    {
    }

    public enum Type
    {
        NONE(AttributeConfig.OMIT_LABEL),
        VALIDATE_NUMBER("validate-number"),
        VALIDATE_DIGITS("validate-digits"),
        VALIDATE_EMAIL("validate-email"),
        VALIDATE_URL("validate-url"),
        VALIDATE_ALPHA("validate-alpha"),
        VALIDATE_ALPHANUM("validate-alphanum");

        private String value;

        Type(String configValue)
        {
            this.value = configValue;
        }

        @Override
        public String toString()
        {
            return value;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.frontend_class");
    }

    @Override
    public Type getDefault()
    {
        return Type.NONE;
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public String toPhpValue()
    {
        return toPhpString();
    }

    @Override
    public Type[] getPredefinedValues()
    {
        return new Type[] {
            Type.NONE,
            Type.VALIDATE_ALPHA,
            Type.VALIDATE_ALPHANUM,
            Type.VALIDATE_DIGITS,
            Type.VALIDATE_EMAIL,
            Type.VALIDATE_NUMBER,
            Type.VALIDATE_URL,
        };
    }

    @Override
    public FrontendClassCombo getUiComponent()
    {
        return new FrontendClassCombo(this);
    }

    public class FrontendClassCombo extends JComboBox<FrontendClass.Type>
    {
        private FrontendClass attrConfig;

        FrontendClassCombo(FrontendClass attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(attrConfig.getDefault());
        }

        public FrontendClass getAttrConfig()
        {
            return attrConfig;
        }
    }
}
