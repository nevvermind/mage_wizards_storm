/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

public class FrontendInput
    extends AttributeConfig<FrontendInput.Type>
    implements IPredefinedValues<FrontendInput.Type>,
               IUiRenderable<FrontendInput.FrontendInputCombo>
{
    public static final String COL  = "frontend_input";
    public static final String CODE = "input";

    public FrontendInput(Type value)
    {
        setValue(value);
    }

    public FrontendInput()
    {
    }

    public enum Type
    {
        NONE(AttributeConfig.OMIT_LABEL),
        TEXT("text"),
        TEXTAREA("textarea"),
        DATE("date"),
        BOOLEAN("boolean"),
        MULTISELECT("multiselect"),
        SELECT("select"),
        PRICE("price"),
        MEDIA_IMAGE("media_image"),
        WEEE("weee");

        private String value;

        Type(String configValue)
        {
            this.value = configValue;
        }

        @Override
        public String toString()
        {
            return value;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.frontend_input");
    }

    @Override
    public FrontendInput.Type getDefault()
    {
        return Type.TEXT;
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public Type[] getPredefinedValues()
    {
        return new FrontendInput.Type[] {
            Type.NONE,
            Type.TEXT,
            Type.TEXTAREA,
            Type.DATE,
            Type.BOOLEAN,
            Type.MULTISELECT,
            Type.SELECT,
            Type.PRICE,
            Type.MEDIA_IMAGE,
            Type.WEEE
        };
    }

    @Override
    public String toPhpValue()
    {
        return toPhpString();
    }

    @Override
    public FrontendInputCombo getUiComponent()
    {
        return new FrontendInputCombo(this);
    }

    public class FrontendInputCombo extends JComboBox<FrontendInput.Type>
    {
        private FrontendInput attrConfig;

        FrontendInputCombo(FrontendInput attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(attrConfig.getDefault());
        }

        public FrontendInput getAttrConfig()
        {
            return attrConfig;
        }
    }
}
