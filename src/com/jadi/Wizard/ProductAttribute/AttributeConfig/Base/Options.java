/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class Options
    extends AttributeConfig<String>
    implements IUiRenderable<Options.TextArea>
{
    public static final String CODE = "option";

    @Override
    public TextArea getUiComponent()
    {
        return new TextArea(this);
    }

    public class TextArea extends JTextArea
    {
        private Options attrConfig;

        TextArea(Options attrConfig)
        {
            super();
            this.attrConfig = attrConfig;
            this.setColumns(20);
            this.setRows(5);
        }

        public Options getAttrConfig()
        {
            return attrConfig;
        }
    }

    @Override
    public String getColumnName()
    {
        return "";
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return "Options";
    }

    @Override
    public String getDefault()
    {
        return "";
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public boolean canRender()
    {
        return true;
    }

    /**
     * @link http://stackoverflow.com/a/13465022/394589
     */
    public List<String> getOptions()
    {
        List<String> lines = new ArrayList<>();

        if (StringUtils.isBlank(getValue())) {
            return lines;
        }

        BufferedReader rdr = new BufferedReader(new StringReader(getValue()));

        try {
            for (String line = rdr.readLine(); line != null; line = rdr.readLine()) {
                if (StringUtils.isNotBlank(line)) {
                    lines.add(line);
                }
            }
            rdr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }
}
