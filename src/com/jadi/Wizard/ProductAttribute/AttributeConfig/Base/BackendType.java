/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

public class BackendType
    extends AttributeConfig<BackendType.Type>
    implements IPredefinedValues<BackendType.Type>,
               IUiRenderable<BackendType.BackendTypeCombo>
{
    public static final String COL  = "backend_type";
    public static final String CODE = "type";

    public BackendType(Type value)
    {
        setValue(value);
    }

    public BackendType()
    {
    }

    public enum Type
    {
        NONE(AttributeConfig.OMIT_LABEL),
        VARCHAR("varchar"),
        INT("int"),
        TEXT("text"),
        DECIMAL("decimal"),
        DATETIME("datetime"),
        STATIC("static");

        private String value;

        Type(String configValue)
        {
            this.value = configValue;
        }

        @Override
        public String toString()
        {
            return value;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.backend_type");
    }

    @Override
    public Type[] getPredefinedValues()
    {
        return new Type[] {
            Type.NONE,
            Type.VARCHAR,
            Type.INT,
            Type.TEXT,
            Type.DECIMAL,
            Type.DATETIME,
            Type.STATIC
        };
    }

    @Override
    public Type getDefault()
    {
        return Type.NONE;
    }

    @Override
    public boolean isValid()
    {
        return getValue() != null;
    }

    @Override
    public String toPhpValue()
    {
        return toPhpString();
    }

    @Override
    public BackendTypeCombo getUiComponent()
    {
        return new BackendTypeCombo(this);
    }

    public class BackendTypeCombo extends JComboBox<BackendType.Type>
    {
        private BackendType attrConfig;

        BackendTypeCombo(BackendType attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(attrConfig.getDefault());
        }

        public BackendType getAttrConfig()
        {
            return attrConfig;
        }
    }
}
