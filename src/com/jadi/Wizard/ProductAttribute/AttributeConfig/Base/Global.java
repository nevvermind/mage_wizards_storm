/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.AttributeConfig.Base;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IPredefinedValues;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.IUiRenderable;

import javax.swing.*;

/**
 * See the <code>\Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_*</code> constants.
 * For some reason, the constants were created in Mage_Catalog.
 */
public class Global
    extends AttributeConfig<Global.Scope>
    implements IPredefinedValues<Global.Scope>,
               IUiRenderable<Global.GlobalCombo>
{
    public static final String COL  = "is_global";
    public static final String CODE = "global";

    public Global(Scope value)
    {
        setValue(value);
    }

    public Global()
    {
    }

    public enum Scope
    {
        STORE("Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE", "Store"),
        GLOBAL("Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL", "Global"),
        WEBSITE("Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE", "Website");

        private String fullPhpConstantName;
        private String friendlyName;

        Scope(String fullPhpConstantName, String friendlyName)
        {
            this.fullPhpConstantName = fullPhpConstantName;
            this.friendlyName = friendlyName;
        }

        @Override
        public String toString()
        {
            return toFriendlyName();
        }

        public String toPhpConstantName()
        {
            return fullPhpConstantName;
        }

        public String toFriendlyName()
        {
            return friendlyName;
        }
    }

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public String getLabelText()
    {
        return I18N.getString("attr.config.pretty_description.global");
    }

    @Override
    public Scope getDefault()
    {
        return Scope.GLOBAL;
    }

    @Override
    public boolean isValid()
    {
        return true;
    }

    @Override
    public Scope[] getPredefinedValues()
    {
        return new Global.Scope[] {
            Scope.GLOBAL,
            Scope.WEBSITE,
            Scope.STORE
        };
    }

    @Override
    public String toPhpValue()
    {
        return getValue().toPhpConstantName();
    }

    @Override
    public GlobalCombo getUiComponent()
    {
        return new GlobalCombo(this);
    }

    public class GlobalCombo extends JComboBox<Global.Scope>
    {
        private Global attrConfig;

        GlobalCombo(Global attrConfig)
        {
            super(attrConfig.getPredefinedValues());
            this.attrConfig = attrConfig;
            this.setSelectedItem(attrConfig.getDefault());
        }

        public Global getAttrConfig()
        {
            return attrConfig;
        }
    }
}
