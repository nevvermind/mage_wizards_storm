/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Utils.Registry;
import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisManager;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.PhpArrayRenderer;
import com.intellij.ui.components.JBTabbedPane;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;

public class AttributeDialog extends JDialog
{
    private final Attribute attribute;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTabbedPane tabbedPane1;
    private JPanel baseAttributeConfigsPanel;
    private JPanel catalogAttributeConfigsPanel;
    private JEditorPane codeSnippetEditorPane;
    private JEditorPane infoEditorPane;
    private JPanel codeSnippetPanel;
    private JPanel infoPanel;
    private JPanel catalogConfigsPanel;
    private JPanel baseConfigsPanel;
    private JPanel preselectionsPanel;

    public AttributeDialog(@NotNull Attribute attribute)
    {
        this.attribute = attribute;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK()
    {
// add your code here
        dispose();
    }

    private void onCancel()
    {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args)
    {
        Registry.set("attribute", new Attribute());
        AttributeDialog dialog = new AttributeDialog(Registry.<Attribute>get("attribute"));
        dialog.pack();
        dialog.setLocationRelativeTo(null); // center on screen
        dialog.setVisible(true);
    }

    private void createUIComponents()
    {
        AnalysisManager analysisManager = AnalysisManager.withAllAnalyses(this.attribute);

        tabbedPane1 = new JBTabbedPane();

        ChangeListener changeListener = new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
                int index = sourceTabbedPane.getSelectedIndex();

                // TODO - really?! find another way to grab the editors.
                if (index == 4) {
                    ((CodeSnippetTextEditor) codeSnippetEditorPane).refreshContent();
                } else if (index == 3) {
                    ((InfoTextEditor) infoEditorPane).refreshContent();
                }
            }
        };
        tabbedPane1.addChangeListener(changeListener);

        infoEditorPane = new InfoTextEditor(this.attribute, analysisManager);

        baseAttributeConfigsPanel = new BaseAttributeConfigsPanel(this.attribute);
        catalogAttributeConfigsPanel = new CatalogAttributeConfigsPanel(this.attribute);

        // store them as we'll need to query for their components later on
        Registry.set("base_config_panel", baseAttributeConfigsPanel);
        Registry.set("catalog_config_panel", catalogAttributeConfigsPanel);

        PhpArrayRenderer phpArrayRenderer = new PhpArrayRenderer();
        codeSnippetEditorPane = new CodeSnippetTextEditor(this.attribute, phpArrayRenderer);

        preselectionsPanel = new PreselectionsPanel(this.attribute);
    }
}
