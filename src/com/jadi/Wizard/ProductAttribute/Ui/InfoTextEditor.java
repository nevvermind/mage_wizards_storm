/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Utils.Registry;
import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisManager;
import com.jadi.Wizard.ProductAttribute.Analysis.AnalysisResult;
import com.jadi.Wizard.ProductAttribute.Attribute;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;

public class InfoTextEditor extends JEditorPane
{
    private final Attribute attribute;
    private final AnalysisManager analysisManager;

    public InfoTextEditor(Attribute attribute, AnalysisManager analysisManager)
    {
        super();
        setEditable(false);
        setContentType("text/html");
        setFont(new java.awt.Font("Tahoma", 0, 12));

        this.attribute = attribute;
        this.analysisManager = analysisManager;
    }

    public void refreshContent()
    {
        Registry.<BaseAttributeConfigsPanel>get("base_config_panel").transferDataToAttribute();
        Registry.<CatalogAttributeConfigsPanel>get("catalog_config_panel").transferDataToAttribute();

        StringBuilder text = new StringBuilder("<html>");

        // clear all previous results
        analysisManager.reset();

        for (AnalysisResult result : analysisManager.getAll()) {
            text.append(renderAnalysisResult(result));
        }

        text.append("</html>");

        setText(text.toString());
    }

    private String renderAnalysisResult(AnalysisResult result)
    {
        return String.format(
            "<strong>%s - %s</strong><br />%s",
            result.getType(),
            result.getTitle(),
            StringUtils.isNotBlank(result.getDescription()) ? "<p>" + result.getDescription() + "</p>" : ""
        );
    }
}
