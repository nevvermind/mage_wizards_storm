/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui.Preselection;

import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.FrontendInput;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Global;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.Group;
import com.jadi.Wizard.ProductAttribute.Ui.BaseAttributeConfigsPanel;
import com.jadi.Wizard.ProductAttribute.Ui.YesNoCombo;
import org.apache.commons.lang.StringUtils;

/**
 * Defined in <code>\Mage_Catalog_Model_Product_Type_Configurable::canUseAttribute()</code>:
 * <code>
 * public function canUseAttribute(Mage_Eav_Model_Entity_Attribute $attribute)
 * {
 *     $allow = $attribute->getIsGlobal() == Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
 *              && $attribute->getIsVisible()
 *              && $attribute->getIsConfigurable()
 *              && $attribute->usesSource()
 *              && $attribute->getIsUserDefined();
 *
 *     return $allow;
 * }
 * </code>
 */
public class ConfigurableSelect extends PreselectAbstract
{
    @Override
    public void select()
    {
        BaseAttributeConfigsPanel basePanel = getBasePanel();

        // "select" frontend input
        basePanel.getFrontendInputComboBox().setSelectedItem(FrontendInput.Type.SELECT);

        // global
        basePanel.getScopeComboBox().setSelectedItem(Global.Scope.GLOBAL);

        // user defined
        basePanel.getUserDefinedComboBox().setSelectedItem(YesNoCombo.Type.YES);

        // because it's user defined, a group MUST be declared
        // otherwise the attribute won't be added to set groups
        if (StringUtils.isBlank(basePanel.getGroupTextField().getText())) {
            basePanel.getGroupTextField().setText(Group.DEFAULT_GROUP_NAME);
        }

        // is configurable
        getCatalogPanel().getConfigurableComboBox().setSelectedItem(YesNoCombo.Type.YES);

        getCatalogPanel().getVisibleComboBox().setSelectedItem(YesNoCombo.Type.YES);
    }

    @Override
    public void deselect()
    {
        new Default().select();
    }

    @Override
    public String getLabel()
    {
        return "Configurable-ready";
    }

    @Override
    public String getDescription()
    {
        return "The configurable attribute must:<br />" +
                "<ul>" +
                "<li>be global</li>" +
                "<li>be visible</li>" +
                "<li>be user defined</li>" +
                "<li>have a group name declared (e.g. \"General\")</li>" +
                "<li>be set as configurable, of course, and</li>" +
                "<li>have a data source; this means either:" +
                "<ul>" +
                "<li>attribute is \"select\"</li>" +
                "<li>attribute is \"multiselect\"</li>" +
                "<li>attribute has a source model declared</li>" +
                "</ul>" +
                "</li>" +
                "</ul>" +
                "This preselection chooses a \"select\" type, " +
                "but you can choose \"multiselect\" or declare a source model.";
    }
}
