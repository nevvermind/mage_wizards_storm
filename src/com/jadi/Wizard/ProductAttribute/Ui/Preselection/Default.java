/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui.Preselection;

import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Comparable;
import com.jadi.Wizard.ProductAttribute.Ui.BaseAttributeConfigsPanel;
import com.jadi.Wizard.ProductAttribute.Ui.CatalogAttributeConfigsPanel;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.*;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.*;

public class Default extends PreselectAbstract
{
    @Override
    public void select()
    {
        BaseAttributeConfigsPanel basePanel = getBasePanel();
        CatalogAttributeConfigsPanel catalogPanel = getCatalogPanel();

        basePanel.getBackendModelComboBox().setSelectedItem(new BackendModel().getDefault());
        basePanel.getBackendTableTextField().setText(new BackendTable().getDefault());
        basePanel.getBackendTypeComboBox().setSelectedItem(new BackendType().getDefault());
        basePanel.getFrontendModelTextField().setText(new FrontendModel().getDefault());
        basePanel.getFrontendInputComboBox().setSelectedItem(new FrontendInput().getDefault());
        basePanel.getFrontendClassComboBox().setSelectedItem(new FrontendClass().getDefault());
        basePanel.getSourceModelComboBox().setSelectedItem(new SourceModel().getDefault());
        basePanel.getRequiredComboBox().setSelectedItem(new IsRequired().getDefault());
        basePanel.getUserDefinedComboBox().setSelectedItem(new IsUserDefined().getDefault());
        basePanel.getUniqueComboBox().setSelectedItem(new IsUnique().getDefault());
        basePanel.getDefaultValueTextField().setText(new DefaultValue().getDefault());
        basePanel.getNoteTextArea().setText(new Note().getDefault());
        basePanel.getScopeComboBox().setSelectedItem(new Global().getDefault());

        catalogPanel.getFrontendInputRendererComboBox().setSelectedItem(new FrontendInputRenderer().getDefault());
        catalogPanel.getVisibleComboBox().setSelectedItem(new Visible().getDefault());
        catalogPanel.getSearchableComboBox().setSelectedItem(new Searchable().getDefault());
        catalogPanel.getFilterableComboBox().setSelectedItem(new Filterable().getDefault());
        catalogPanel.getComparableComboBox().setSelectedItem(new Comparable().getDefault());
        catalogPanel.getVisibleOnFrontComboBox().setSelectedItem(new VisibleOnFront().getDefault());
        catalogPanel.getWysiwygEnabledComboBox().setSelectedItem(new WysiwygEnabled().getDefault());
        catalogPanel.getHtmlAllowedOnFrontComboBox().setSelectedItem(new HtmlAllowedOnFront().getDefault());
        catalogPanel.getVisibleInAdvSearchComboBox().setSelectedItem(new VisibleInAdvancedSearch().getDefault());
        catalogPanel.getFilterableInSearchComboBox().setSelectedItem(new FilterableInSearch().getDefault());
        catalogPanel.getUsedInProductListingComboBox().setSelectedItem(new UsedInProductListing().getDefault());
        catalogPanel.getUsedForSortByComboBox().setSelectedItem(new UsedForSortBy().getDefault());
        catalogPanel.getConfigurableComboBox().setSelectedItem(new Configurable().getDefault());
        catalogPanel.getUsedForPromoRulesComboBox().setSelectedItem(new UsedForPromoRules().getDefault());
        catalogPanel.getPositionTextField().setText(new Position().getDefault());

        catalogPanel.getApplyToList().selectAll();
    }

    @Override
    public void deselect()
    {
        // noop
    }

    @Override
    public String getLabel()
    {
        return "<Reset>";
    }

    @Override
    public String getDescription()
    {
        return "";
    }
}
