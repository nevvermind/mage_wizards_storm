/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Utils.I18N;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.Ui.Preselection.PreselectAbstract;
import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class PreselectionsPanel extends JPanel
{
    private final Attribute attribute;

    public PreselectionsPanel(Attribute attribute)
    {
        this.attribute = attribute;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents()
    {

        preselectionsLabel = new javax.swing.JLabel();
        preselectionsComboBox = PreselectionsComboBox.withAllPreselections(this.attribute);
        preselectionsComboBox.addItemListener(new ChangePreselectionDescription());
        preselectionsLabel.setText(I18N.getString("ui.label.choose_preselection"));

        jScrollPane1 = new JBScrollPane();
        preselectionDescEditorPane = new javax.swing.JEditorPane();

        preselectionDescEditorPane.setEditable(false);
        preselectionDescEditorPane.setContentType("text/html"); // NOI18N
        preselectionDescEditorPane.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        preselectionDescEditorPane.setDragEnabled(false);
        preselectionDescEditorPane.setFocusCycleRoot(false);
        preselectionDescEditorPane.setFocusTraversalKeysEnabled(false);
        preselectionDescEditorPane.setFocusable(false);
        preselectionDescEditorPane.setName("preselectionDesc"); // NOI18N
        preselectionDescEditorPane.setOpaque(false);
        jScrollPane1.setViewportView(preselectionDescEditorPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(preselectionsLabel)
                                        .addComponent(preselectionsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(140, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(preselectionsLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(preselectionsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(134, Short.MAX_VALUE))
        );
    }

    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JEditorPane preselectionDescEditorPane;
    private javax.swing.JComboBox preselectionsComboBox;
    private javax.swing.JLabel preselectionsLabel;

    private class ChangePreselectionDescription implements ItemListener
    {
        @Override
        public void itemStateChanged(ItemEvent e)
        {
            PreselectAbstract item = (PreselectAbstract) e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                preselectionDescEditorPane.setText("<html><body>" + item.getDescription() + "</body></html>");
            }
        }
    }
}
