/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Comparable;
import com.intellij.ui.components.JBScrollPane;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.*;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.util.List;

public class CatalogAttributeConfigsPanel extends javax.swing.JPanel
{
    private final Attribute attribute;

    public CatalogAttributeConfigsPanel(Attribute attribute)
    {
        this.attribute = attribute;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents()
    {
        visibleLabel               = Factory.getLabelForAttrConfigs(new Visible());
        searchableLabel            = Factory.getLabelForAttrConfigs(new Searchable());
        comparableLabel            = Factory.getLabelForAttrConfigs(new com.jadi.Wizard.ProductAttribute.AttributeConfig.Catalog.Comparable());
        visibleOnFrontLabel        = Factory.getLabelForAttrConfigs(new VisibleOnFront());
        wysiwygEnabledLabel        = Factory.getLabelForAttrConfigs(new WysiwygEnabled());
        htmlAllowedOnFrontLabel    = Factory.getLabelForAttrConfigs(new HtmlAllowedOnFront());
        visibleInAdvSearchLabel    = Factory.getLabelForAttrConfigs(new VisibleInAdvancedSearch());
        filterableInSearchLabel    = Factory.getLabelForAttrConfigs(new FilterableInSearch());
        usedInProductListingLabel  = Factory.getLabelForAttrConfigs(new UsedInProductListing());
        usedForSortByLabel         = Factory.getLabelForAttrConfigs(new UsedForSortBy());
        configurableLabel          = Factory.getLabelForAttrConfigs(new Configurable());
        usedForPromoRulesLabel     = Factory.getLabelForAttrConfigs(new UsedForPromoRules());
        filterableLabel            = Factory.getLabelForAttrConfigs(new Filterable());
        positionLabel              = Factory.getLabelForAttrConfigs(new Position());
        frontendInputRendererLabel = Factory.getLabelForAttrConfigs(new FrontendInputRenderer());
        applyToLabel               = Factory.getLabelForAttrConfigs(new ApplyTo());

        visibleComboBox               = Factory.getUiComponent(new Visible());
        searchableComboBox            = Factory.getUiComponent(new Searchable());
        comparableComboBox            = Factory.getUiComponent(new Comparable());
        visibleOnFrontComboBox        = Factory.getUiComponent(new VisibleOnFront());
        filterableInSearchComboBox    = Factory.getUiComponent(new FilterableInSearch());
        visibleInAdvSearchComboBox    = Factory.getUiComponent(new VisibleInAdvancedSearch());
        htmlAllowedOnFrontComboBox    = Factory.getUiComponent(new HtmlAllowedOnFront());
        wysiwygEnabledComboBox        = Factory.getUiComponent(new WysiwygEnabled());
        usedInProductListingComboBox  = Factory.getUiComponent(new UsedInProductListing());
        usedForSortByComboBox         = Factory.getUiComponent(new UsedForSortBy());
        usedForPromoRulesComboBox     = Factory.getUiComponent(new UsedForPromoRules());
        configurableComboBox          = Factory.getUiComponent(new Configurable());
        filterableComboBox            = Factory.getUiComponent(new Filterable());
        positionTextField             = Factory.getUiComponent(new Position());
        frontendInputRendererComboBox = Factory.getUiComponent(new FrontendInputRenderer());
        jScrollPane1 = new JBScrollPane();
        applyToList = new ApplyTo().getUiComponent();

        jScrollPane1.setViewportView(applyToList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(positionLabel)
                                                .addGap(115, 115, 115)
                                                .addComponent(positionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(visibleLabel)
                                        .addComponent(searchableLabel)
                                        .addComponent(comparableLabel)
                                        .addComponent(visibleOnFrontLabel)
                                        .addComponent(wysiwygEnabledLabel)
                                        .addComponent(htmlAllowedOnFrontLabel)
                                        .addComponent(visibleInAdvSearchLabel)
                                        .addComponent(filterableInSearchLabel)
                                        .addComponent(usedInProductListingLabel)
                                        .addComponent(usedForSortByLabel)
                                        .addComponent(configurableLabel)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(frontendInputRendererLabel)
                                                        .addComponent(applyToLabel))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(18, 18, 18)
                                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(frontendInputRendererComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(usedForPromoRulesLabel)
                                                        .addComponent(filterableLabel))
                                                .addGap(29, 29, 29)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(filterableComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(usedForPromoRulesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(170, 170, 170)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(configurableComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(usedForSortByComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(usedInProductListingComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(searchableComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(comparableComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(visibleOnFrontComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(wysiwygEnabledComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(htmlAllowedOnFrontComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(visibleInAdvSearchComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(filterableInSearchComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(visibleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addContainerGap(120, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(visibleLabel)
                                        .addComponent(visibleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(searchableLabel)
                                        .addComponent(searchableComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(comparableLabel)
                                        .addComponent(comparableComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(visibleOnFrontLabel)
                                        .addComponent(visibleOnFrontComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(wysiwygEnabledLabel)
                                        .addComponent(wysiwygEnabledComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(htmlAllowedOnFrontLabel)
                                        .addComponent(htmlAllowedOnFrontComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(visibleInAdvSearchLabel)
                                        .addComponent(visibleInAdvSearchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(filterableInSearchLabel)
                                        .addComponent(filterableInSearchComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(usedInProductListingLabel)
                                        .addComponent(usedInProductListingComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(usedForSortByLabel)
                                        .addComponent(usedForSortByComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(configurableLabel)
                                        .addComponent(configurableComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(usedForPromoRulesLabel)
                                        .addComponent(usedForPromoRulesComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(filterableComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(filterableLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(positionLabel)
                                        .addComponent(positionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(frontendInputRendererLabel)
                                        .addComponent(frontendInputRendererComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(applyToLabel)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(10, Short.MAX_VALUE))
        );
    }

    private javax.swing.JLabel applyToLabel;
    private javax.swing.JLabel comparableLabel;
    private javax.swing.JLabel configurableLabel;
    private javax.swing.JLabel filterableInSearchLabel;
    private javax.swing.JLabel filterableLabel;
    private javax.swing.JLabel htmlAllowedOnFrontLabel;
    private javax.swing.JLabel frontendInputRendererLabel;
    private javax.swing.JLabel positionLabel;
    private javax.swing.JLabel searchableLabel;
    private javax.swing.JLabel usedForSortByLabel;
    private javax.swing.JLabel usedForPromoRulesLabel;
    private javax.swing.JLabel usedInProductListingLabel;
    private javax.swing.JLabel visibleInAdvSearchLabel;
    private javax.swing.JLabel visibleLabel;
    private javax.swing.JLabel visibleOnFrontLabel;
    private javax.swing.JLabel wysiwygEnabledLabel;

    private javax.swing.JScrollPane jScrollPane1;
    private ApplyTo.List applyToList;
    private javax.swing.JTextField positionTextField;
    private Filterable.ComboBox filterableComboBox;
    private FrontendInputRenderer.ComboBox frontendInputRendererComboBox;
    private YesNoCombo comparableComboBox;
    private YesNoCombo configurableComboBox;
    private YesNoCombo filterableInSearchComboBox;
    private YesNoCombo htmlAllowedOnFrontComboBox;
    private YesNoCombo searchableComboBox;
    private YesNoCombo usedForPromoRulesComboBox;
    private YesNoCombo usedForSortByComboBox;
    private YesNoCombo usedInProductListingComboBox;
    private YesNoCombo visibleComboBox;
    private YesNoCombo visibleInAdvSearchComboBox;
    private YesNoCombo visibleOnFrontComboBox;
    private YesNoCombo wysiwygEnabledComboBox;

    public ApplyTo.List getApplyToList()
    {
        return applyToList;
    }

    public JTextField getPositionTextField()
    {
        return positionTextField;
    }

    public Filterable.ComboBox getFilterableComboBox()
    {
        return filterableComboBox;
    }

    public FrontendInputRenderer.ComboBox getFrontendInputRendererComboBox()
    {
        return frontendInputRendererComboBox;
    }

    public YesNoCombo getComparableComboBox()
    {
        return comparableComboBox;
    }

    public YesNoCombo getConfigurableComboBox()
    {
        return configurableComboBox;
    }

    public YesNoCombo getFilterableInSearchComboBox()
    {
        return filterableInSearchComboBox;
    }

    public YesNoCombo getHtmlAllowedOnFrontComboBox()
    {
        return htmlAllowedOnFrontComboBox;
    }

    public YesNoCombo getSearchableComboBox()
    {
        return searchableComboBox;
    }

    public YesNoCombo getUsedForPromoRulesComboBox()
    {
        return usedForPromoRulesComboBox;
    }

    public YesNoCombo getUsedForSortByComboBox()
    {
        return usedForSortByComboBox;
    }

    public YesNoCombo getUsedInProductListingComboBox()
    {
        return usedInProductListingComboBox;
    }

    public YesNoCombo getVisibleComboBox()
    {
        return visibleComboBox;
    }

    public YesNoCombo getVisibleInAdvSearchComboBox()
    {
        return visibleInAdvSearchComboBox;
    }

    public YesNoCombo getVisibleOnFrontComboBox()
    {
        return visibleOnFrontComboBox;
    }

    public YesNoCombo getWysiwygEnabledComboBox()
    {
        return wysiwygEnabledComboBox;
    }

    public void transferDataToAttribute()
    {
        setValueIfNotEmptyText(new Position(), getPositionTextField());

        attribute.addConfig(new Filterable((Filterable.Type) getFilterableComboBox().getSelectedItem()));
        attribute.addConfig(new FrontendInputRenderer((FrontendInputRenderer.Type) getFrontendInputRendererComboBox().getSelectedItem()));

        List<ApplyTo.Type> selectedApplyTo = getApplyToList().getSelectedValuesList();
        AttributeConfig<ApplyTo.Type[]> attributeConfig = new ApplyTo().setValue(selectedApplyTo.toArray(new ApplyTo.Type[selectedApplyTo.size()]));
        attribute.addConfig(attributeConfig);

        transferComboSelection(new Configurable(), getConfigurableComboBox());
        transferComboSelection(new Comparable(), getComparableComboBox());
        transferComboSelection(new FilterableInSearch(), getFilterableInSearchComboBox());
        transferComboSelection(new HtmlAllowedOnFront(), getHtmlAllowedOnFrontComboBox());
        transferComboSelection(new Searchable(), getSearchableComboBox());
        transferComboSelection(new UsedForPromoRules(), getUsedForPromoRulesComboBox());
        transferComboSelection(new UsedForSortBy(), getUsedForSortByComboBox());
        transferComboSelection(new UsedInProductListing(), getUsedInProductListingComboBox());
        transferComboSelection(new Visible(), getVisibleComboBox());
        transferComboSelection(new VisibleInAdvancedSearch(), getVisibleInAdvSearchComboBox());
        transferComboSelection(new VisibleOnFront(), getVisibleOnFrontComboBox());
        transferComboSelection(new WysiwygEnabled(), getWysiwygEnabledComboBox());
    }

    private <T extends AttributeConfig<YesNoCombo.Type>> void transferComboSelection(T attrConfig, JComboBox<YesNoCombo.Type> comboBox)
    {
        attrConfig.setValue((YesNoCombo.Type) comboBox.getSelectedItem());
        attribute.addConfig(attrConfig);
    }

    private void setValueIfNotEmptyText(AttributeConfig attrConfig, JTextComponent textField)
    {
        if (!textField.getText().isEmpty()) {
            attrConfig.setValue(textField.getText());
            attribute.addConfig(attrConfig);
        } else {
            attribute.removeConfig(attrConfig);
        }
    }
}
