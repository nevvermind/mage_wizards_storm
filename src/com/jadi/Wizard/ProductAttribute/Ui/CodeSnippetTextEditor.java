/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Utils.Registry;
import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeRenderer;

import javax.swing.*;

public class CodeSnippetTextEditor extends JEditorPane
{
    private final Attribute attribute;
    private final AttributeRenderer attributeRenderer;

    CodeSnippetTextEditor(Attribute attribute, AttributeRenderer attributeRenderer)
    {
        super();
        this.attribute = attribute;
        this.attributeRenderer = attributeRenderer;
        setEditable(false);
        setFont(new java.awt.Font("Courier New", 0, 12));

        // TODO - set scroll bars
    }

    public void refreshContent()
    {
        Registry.<BaseAttributeConfigsPanel>get("base_config_panel").transferDataToAttribute();
        Registry.<CatalogAttributeConfigsPanel>get("catalog_config_panel").transferDataToAttribute();

        this.setText(this.attributeRenderer.render(this.attribute));
    }
}
