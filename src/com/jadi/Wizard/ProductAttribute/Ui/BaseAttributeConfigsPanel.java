/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Wizard.ProductAttribute.Attribute;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;
import com.intellij.ui.components.JBScrollPane;
import com.jadi.Wizard.ProductAttribute.AttributeConfig.Base.*;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

public class BaseAttributeConfigsPanel extends javax.swing.JPanel
{
    private final Attribute attribute;

    public BaseAttributeConfigsPanel(Attribute attribute)
    {
        this.attribute = attribute;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    private void initComponents()
    {
        codeLabel = new JLabel("Code:");
        frontendLabelLabel   = Factory.getLabelForAttrConfigs(new Label());
        frontendInputLabel   = Factory.getLabelForAttrConfigs(new FrontendInput());
        frontendModelLabel   = Factory.getLabelForAttrConfigs(new FrontendModel());
        frontendClassLabel   = Factory.getLabelForAttrConfigs(new FrontendClass());
        backendTypeLabel     = Factory.getLabelForAttrConfigs(new BackendType());
        backendTableLabel    = Factory.getLabelForAttrConfigs(new BackendTable());
        backendModelLabel    = Factory.getLabelForAttrConfigs(new BackendModel());
        sourceModelLabel     = Factory.getLabelForAttrConfigs(new SourceModel());
        groupLabel           = Factory.getLabelForAttrConfigs(new Group());
        requiredLabel        = Factory.getLabelForAttrConfigs(new IsRequired());
        userDefinedLabel     = Factory.getLabelForAttrConfigs(new IsUserDefined());
        noteLabel            = Factory.getLabelForAttrConfigs(new Note());
        scopeInputLabel      = Factory.getLabelForAttrConfigs(new Global());
        uniqueLabel          = Factory.getLabelForAttrConfigs(new IsUnique());
        defaultValueLabel    = Factory.getLabelForAttrConfigs(new DefaultValue());
        optionsLabel         = new JLabel("Options:");
        noteCharCounterLabel = new JLabel(String.format("(%d)", Note.MAX_LENGTH));
        onePerLineCommentLabel = new JLabel("(one per line)");

        jScrollPane2           = new JBScrollPane();
        jScrollPane3           = new JBScrollPane();
        codeTextField          = new AttributeCodeTextField();
        frontendLabelTextField = Factory.getUiComponent(new Label());
        frontendInputComboBox  = Factory.getUiComponent(new FrontendInput());
        frontendModelTextField = Factory.getUiComponent(new FrontendModel());
        frontendClassComboBox  = Factory.getUiComponent(new FrontendClass());
        backendTypeComboBox    = Factory.getUiComponent(new BackendType());
        backendModelComboBox   = Factory.getUiComponent(new BackendModel());
        sourceModelComboBox    = Factory.getUiComponent(new SourceModel());
        groupTextField         = Factory.getUiComponent(new Group());
        requiredComboBox       = Factory.getUiComponent(new IsRequired());
        userDefinedComboBox    = Factory.getUiComponent(new IsUserDefined());
        noteTextArea           = Factory.getUiComponent(new Note());
        backendTableTextField  = Factory.getUiComponent(new BackendTable());
        scopeComboBox          = Factory.getUiComponent(new Global());
        uniqueComboBox         = Factory.getUiComponent(new IsUnique());
        defaultValueTextField  = Factory.getUiComponent(new DefaultValue());
        optionsTextArea        = Factory.getUiComponent(new Options());

        jScrollPane3.setViewportView(optionsTextArea);

        jScrollPane2.setViewportView(noteTextArea);

        noteTextArea.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void insertUpdate(DocumentEvent e)
            {
                updateText(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e)
            {
                updateText(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e)
            {
                updateText(e);
            }

            private void updateText(DocumentEvent e)
            {
                if (e.getDocument() instanceof TextComponentLimiterDecorator.LimitDocument) {
                    TextComponentLimiterDecorator.LimitDocument doc = (TextComponentLimiterDecorator.LimitDocument) e.getDocument();
                    int limit = doc.getLimit();
                    int remaining = limit - e.getDocument().getLength();
                    noteCharCounterLabel.setText("(" + Integer.toString(remaining) + ")");
                }
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(frontendLabelLabel)
                                                                        .addComponent(frontendInputLabel)
                                                                        .addComponent(frontendClassLabel)
                                                                        .addComponent(backendTypeLabel)
                                                                        .addComponent(backendModelLabel)
                                                                        .addComponent(backendTableLabel)
                                                                        .addComponent(sourceModelLabel)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(onePerLineCommentLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                .addGap(13, 13, 13)))
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addGap(22, 22, 22)
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(groupTextField)
                                                                                        .addComponent(sourceModelComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                        .addComponent(backendModelComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                        .addComponent(backendTableTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(defaultValueTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addGroup(layout.createSequentialGroup()
                                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                        .addComponent(uniqueComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                                                                .addComponent(requiredComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                                                .addComponent(userDefinedComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, 128, Short.MAX_VALUE)))
                                                                                                .addGap(0, 0, Short.MAX_VALUE))
                                                                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)))
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addGap(23, 23, 23)
                                                                                .addComponent(backendTypeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(frontendModelLabel)
                                                                        .addComponent(codeLabel))
                                                                .addGap(19, 19, 19)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(frontendLabelTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(frontendInputComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(frontendModelTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(frontendClassComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(codeTextField)
                                                                        .addComponent(scopeComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(requiredLabel)
                                                                        .addComponent(userDefinedLabel)
                                                                        .addComponent(groupLabel))
                                                                .addGap(0, 0, Short.MAX_VALUE)))
                                                .addGap(16, 16, 16))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(noteLabel)
                                                        .addComponent(noteCharCounterLabel)
                                                        .addComponent(scopeInputLabel)
                                                        .addComponent(defaultValueLabel)
                                                        .addComponent(uniqueLabel)
                                                        .addComponent(optionsLabel))
                                                .addGap(391, 391, 391))))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(codeLabel)
                                        .addComponent(codeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(frontendLabelTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(frontendLabelLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(frontendInputComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(frontendInputLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(scopeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(scopeInputLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(frontendModelLabel)
                                        .addComponent(frontendModelTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addComponent(frontendClassComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(frontendClassLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(backendTypeLabel)
                                        .addComponent(backendTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(backendTableLabel)
                                        .addComponent(backendTableTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(backendModelLabel)
                                        .addComponent(backendModelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(sourceModelLabel)
                                        .addComponent(sourceModelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(groupLabel)
                                        .addComponent(groupTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(defaultValueLabel)
                                        .addComponent(defaultValueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(requiredLabel)
                                        .addComponent(requiredComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(userDefinedLabel)
                                        .addComponent(userDefinedComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(uniqueLabel)
                                        .addComponent(uniqueComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(noteLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(noteCharCounterLabel))
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(optionsLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(onePerLineCommentLabel))
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(8, Short.MAX_VALUE))
        );
    }

    private JLabel backendModelLabel;
    private JLabel backendTableLabel;
    private JLabel backendTypeLabel;
    private JLabel codeLabel;
    private JLabel frontendClassLabel;
    private JLabel frontendInputLabel;
    private JLabel frontendLabelLabel;
    private JLabel frontendModelLabel;
    private JLabel groupLabel;
    private JLabel noteCharCounterLabel;
    private JLabel noteLabel;
    private JLabel requiredLabel;
    private JLabel sourceModelLabel;
    private JLabel userDefinedLabel;
    private JLabel scopeInputLabel;
    private JLabel uniqueLabel;
    private JLabel defaultValueLabel;
    private JLabel onePerLineCommentLabel;
    private JLabel optionsLabel;

    private JTextField backendTableTextField;
    private JTextField codeTextField;
    private JTextField frontendLabelTextField;
    private JTextField frontendModelTextField;
    private JTextField groupTextField;
    private JTextField defaultValueTextField;

    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;
    private JTextArea noteTextArea;
    private JTextArea optionsTextArea;
    private BackendModel.BackendModelCombo backendModelComboBox;
    private BackendType.BackendTypeCombo backendTypeComboBox;
    private FrontendClass.FrontendClassCombo frontendClassComboBox;
    private FrontendInput.FrontendInputCombo frontendInputComboBox;
    private SourceModel.SourceModelCombo sourceModelComboBox;
    private Global.GlobalCombo scopeComboBox;
    private YesNoCombo requiredComboBox;
    private YesNoCombo userDefinedComboBox;
    private YesNoCombo uniqueComboBox;

    public JTextField getBackendTableTextField()
    {
        return backendTableTextField;
    }

    public JTextField getCodeTextField()
    {
        return codeTextField;
    }

    public JTextField getFrontendLabelTextField()
    {
        return frontendLabelTextField;
    }

    public JTextField getFrontendModelTextField()
    {
        return frontendModelTextField;
    }

    public JTextField getGroupTextField()
    {
        return groupTextField;
    }

    public JTextArea getNoteTextArea()
    {
        return noteTextArea;
    }

    public BackendModel.BackendModelCombo getBackendModelComboBox()
    {
        return backendModelComboBox;
    }

    public BackendType.BackendTypeCombo getBackendTypeComboBox()
    {
        return backendTypeComboBox;
    }

    public FrontendClass.FrontendClassCombo getFrontendClassComboBox()
    {
        return frontendClassComboBox;
    }

    public FrontendInput.FrontendInputCombo getFrontendInputComboBox()
    {
        return frontendInputComboBox;
    }

    public YesNoCombo getRequiredComboBox()
    {
        return requiredComboBox;
    }

    public SourceModel.SourceModelCombo getSourceModelComboBox()
    {
        return sourceModelComboBox;
    }

    public YesNoCombo getUserDefinedComboBox()
    {
        return userDefinedComboBox;
    }

    public Global.GlobalCombo getScopeComboBox()
    {
        return scopeComboBox;
    }

    public YesNoCombo getUniqueComboBox()
    {
        return uniqueComboBox;
    }

    public JTextField getDefaultValueTextField()
    {
        return defaultValueTextField;
    }

    public JTextArea getOptionsTextArea()
    {
        return optionsTextArea;
    }

    public void transferDataToAttribute()
    {
        attribute.setCode(getCodeTextField().getText());

        setValueIfNotEmptyText(new FrontendModel(), getFrontendModelTextField());
        setValueIfNotEmptyText(new BackendTable(), getBackendTableTextField());
        setValueIfNotEmptyText(new Label(), getFrontendLabelTextField());
        setValueIfNotEmptyText(new DefaultValue(), getDefaultValueTextField());
        setValueIfNotEmptyText(new Note(), getNoteTextArea());
        setValueIfNotEmptyText(new Group(), getGroupTextField());
        setValueIfNotEmptyText(new Options(), getOptionsTextArea());

        attribute.addConfig(new BackendModel((BackendModel.Type) getBackendModelComboBox().getSelectedItem()));
        attribute.addConfig(new BackendType((BackendType.Type) getBackendTypeComboBox().getSelectedItem()));
        attribute.addConfig(new FrontendInput((FrontendInput.Type) getFrontendInputComboBox().getSelectedItem()));
        attribute.addConfig(new FrontendClass((FrontendClass.Type) getFrontendClassComboBox().getSelectedItem()));
        attribute.addConfig(new SourceModel((SourceModel.Type) getSourceModelComboBox().getSelectedItem()));
        attribute.addConfig(new Global((Global.Scope) getScopeComboBox().getSelectedItem()));

        attribute.addConfig(new IsRequired().setValue((YesNoCombo.Type) getRequiredComboBox().getSelectedItem()));
        attribute.addConfig(new IsUserDefined().setValue((YesNoCombo.Type) getUserDefinedComboBox().getSelectedItem()));
        attribute.addConfig(new IsUnique().setValue((YesNoCombo.Type) getUniqueComboBox().getSelectedItem()));
    }

    private void setValueIfNotEmptyText(AttributeConfig attrConfig, JTextComponent textField)
    {
        if (StringUtils.isNotBlank(textField.getText())) {
            attrConfig.setValue(textField.getText());
            attribute.addConfig(attrConfig);
        } else {
            attribute.removeConfig(attrConfig);
        }
    }
}
