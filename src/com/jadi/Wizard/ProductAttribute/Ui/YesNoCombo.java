/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 giadiireg@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.jadi.Wizard.ProductAttribute.Ui;

import com.jadi.Wizard.ProductAttribute.AttributeConfig.AttributeConfig;

import javax.swing.*;

// TODO - change all to ComboBox? JComboBox is deprecated. try it
public class YesNoCombo extends JComboBox<YesNoCombo.Type>
{
    private AttributeConfig attrConfig;

    public enum Type
    {
        NONE(AttributeConfig.OMIT_LABEL, ""),
        YES("Yes", "true"),
        NO("No", "false");

        private final String stringValue;
        private final String phpBooleanValue;

        Type(String stringValue, String phpBooleanValue)
        {
            this.stringValue = stringValue;
            this.phpBooleanValue = phpBooleanValue;
        }

        public String getPhpBooleanValue()
        {
            return phpBooleanValue;
        }

        @Override
        public String toString()
        {
            return stringValue;
        }
    }

    public YesNoCombo(AttributeConfig attrConfig)
    {
        super(new Type[] {Type.YES, Type.NO, Type.NONE});
        this.attrConfig = attrConfig;
        this.setSelectedItem(this.attrConfig.getDefault());
    }

    public AttributeConfig getAttrConfig()
    {
        return attrConfig;
    }
}
